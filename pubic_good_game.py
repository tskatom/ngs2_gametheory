
"""
public-goods contribution games.
"""

import meanstat

class PublicGoodGame(meanstat.MeanStatisticGame):
    def __init__(self, N, min_choice, max_choice, step_choice, multi_factor):
        meanstat.MeanStatisticGame.__init__(self, N,
                                            min_choice, max_choice, step_choice)
        self.multi_factor = multi_factor

    @property
    def title(self):
        return "public goods game with N=%d, multi_factor=%s" % \
               (self.N, self.multi_factor)

    def payoff(self, own, others):
        return (own + others) * 1.0 * self.multi_factor / self.N - own

game = PublicGoodGame(3, 0, 10, 1, 2)
p = game.mixed_strategy_profile()
print p.strategy_value(10)
